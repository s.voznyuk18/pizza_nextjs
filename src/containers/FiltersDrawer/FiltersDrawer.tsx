'use client'
import { useState, useCallback } from 'react';
import { Drawer } from '@/ComponentsRoot';
import { Filters } from '@/ContainersRoot';


export const FiltersDrawer = () => {

  const [open, setOpen] = useState(false);

  const handleOpen = useCallback(() => setOpen(true), []);

  const handleClose = useCallback(() => setOpen(false), []);
  return (
    <>
      <button type="button" onClick={handleOpen}>handleOpen</button>
      <Drawer
        anchor="left"
        open={open}
        onClose={handleClose}
      >
        <Filters />
      </Drawer>
    </>
  )
}

export default FiltersDrawer;
