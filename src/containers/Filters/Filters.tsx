'use client'
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import { usePathname } from 'next/navigation';

import { Accordion, FilterList } from '@/ComponentsRoot';
import style from './filtersBar.module.scss';
import { IFilter } from '@/TypesRoot';
import { getQueryStringFromObject, getObjectFromQueryString, toggleFiltres } from '@/UtilsRoot';

const test = {
  categories: [
    {
      id: 1,
      name: 'Все',
      href: 'all'
    },
    {
      id: 2,
      name: 'Мясные',
      href: 'meat'
    },
    {
      id: 3,
      name: 'Вегетарианская',
      href: 'vegeterian'
    },
    {
      id: 4,
      name: 'Гриль',
      href: 'greel'
    },
  ],
  types: [
    {
      id: 1,
      name: "first",
      href: 'first'
    },
    {
      id: 2,
      name: "second",
      href: 'second'
    }
  ]
}

const Filters = () => {
  const pathname = usePathname();
  const router = useRouter();
  const [chosenFilter, setChosenFilter] = useState<IFilter<string[]>>({});

  useEffect(() => {
    const queryString = getQueryStringFromObject(chosenFilter);
    router.push(`/${queryString}`, { scroll: false })
  }, [chosenFilter, router])

  useEffect(() => {
    const filterObject = getObjectFromQueryString(pathname);
    setChosenFilter(filterObject);
  }, [pathname]);

  return (
    <>
      <Accordion
        title="Categories"
      >
        <FilterList
          filterList={test?.categories}
          filter={chosenFilter["Categories"]}
          handleToggleFiltres={(filterName) => toggleFiltres('Categories', setChosenFilter, filterName)}
        />
      </Accordion>
      <Accordion
        title="types"
      >
        <FilterList
          filterList={test?.types}
          filter={chosenFilter["types"]}
          handleToggleFiltres={(filterName) => toggleFiltres('types', setChosenFilter, filterName)}
        />
      </Accordion>
    </>
  )
}

export default Filters;