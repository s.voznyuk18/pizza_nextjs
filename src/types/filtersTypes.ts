import { Dispatch, SetStateAction } from 'react'

export interface IFilter<T> {
  [key: string]: T;
}

export type TToggleFilter = (filterType: string, setFilterCb: Dispatch<SetStateAction<IFilter<string[]>>>, filterName: string) => void;
