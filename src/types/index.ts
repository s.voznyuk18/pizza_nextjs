import { IFilter, TToggleFilter } from './filtersTypes';

export type { IFilter, TToggleFilter };