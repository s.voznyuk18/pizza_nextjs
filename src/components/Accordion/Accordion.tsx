import React, { useId } from 'react';
import style from './Accordion.module.scss';

interface IProps {
  title: string;
  children: React.ReactNode;
}

const Accordion = ({ title, children }: IProps): React.JSX.Element => {
  const id = useId()

  return (
    <section className={style.accordion}>
      <input type="checkbox" name={`accordion_${title}_${id}`} id={`accordion_${title}_${id}`} defaultChecked />
      <label htmlFor={`accordion_${title}_${id}`} className={style.tab__label}>{title}</label>
      <div className={style.tab__content}>
        {children}
      </div>
    </section>
  )
}

export default Accordion;
