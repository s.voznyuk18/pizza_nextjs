import React from "react";
import classes from "./Drawer.module.scss";
import { changeAnchor } from "./helpers";

interface IProps {
  open: boolean;
  anchor: "left" | "right";
  children: React.ReactNode;
  onClose: () => void;
}

export const Drawer = (props: IProps) => {
  const { open, anchor, children, onClose } = props;
  const {
    drawer,
    animate,
    hidden,
    overlay,
    overlayOpen,
    overlayHidden,
    header
  } = classes;

  return (
    <>
      <div
        className={`${overlay} ${!open && overlayHidden} ${open && overlayOpen
          }`}
        onClick={onClose}
        aria-hidden="true"
      />
      <div
        tabIndex={-1}
        className={`${drawer} ${open && animate} ${!open && hidden
          } ${changeAnchor(anchor, classes)}`}
      >
        <div className={header} />
        {children}
      </div>
    </>
  );
};

export default Drawer;