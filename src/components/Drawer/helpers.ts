export const changeAnchor = (anchor: "left" | "right", classes: { [key: string]: string }) => {
  switch (anchor) {
    case "left":
      return classes.left;
    case "right":
      return classes.right;
    default:
      return classes.left;
  }
};
