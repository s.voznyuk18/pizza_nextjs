'use client'
import React, { memo } from 'react'
import style from './FilterList.module.scss';

interface IProps {
  filter: string[];
  handleToggleFiltres: (filterName: string) => void
  filterList: {
    id: number;
    name: string;
    href: string;
  }[];
}

const FilterList = ({ filter, filterList, handleToggleFiltres }: IProps) => {
  return (
    <ul className={style.filterList}>
      {
        filterList && filterList.map(item => (
          <li
            key={item?.id}
            className={`${style.filterItem} ${filter?.includes(item?.href) ? style.filterItem__active : ''}`}
            onClick={() => handleToggleFiltres(item?.href)}
          >
            <p>{item?.name}</p>
          </li>
        ))
      }
    </ul>
  )
}

export default memo(FilterList);
