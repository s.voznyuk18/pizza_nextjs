import queryString from 'query-string';
import { isEmpty } from 'lodash';

import { IFilter, TToggleFilter } from '@/TypesRoot';

export const getQueryStringFromObject = (filter: IFilter<string[]>) => {
  const deletedEmptyFilter = {} as IFilter<string[]>;

  for (let key in filter) {
    if (!isEmpty(filter[key])) deletedEmptyFilter[key] = filter[key];
  }

  const stringified = queryString.stringify(deletedEmptyFilter, { arrayFormat: 'comma' });
  return stringified;
};

export const getObjectFromQueryString = (search: string): IFilter<string[]> => {
  const parsed = queryString.parse(search.slice(1)) as IFilter<string>;
  const paerseObjValueToArr = {} as IFilter<string[]>;

  for (let key in parsed) {
    paerseObjValueToArr[key] = parsed[key].split(',');
  }

  return paerseObjValueToArr;
};

export const toggleFiltres: TToggleFilter = (filterType, setFilterCb, filterName) => {
  setFilterCb(prevFilter => {
    const currentFilter = prevFilter[filterType] || [];

    const updatedFilter = currentFilter.includes(filterName)
      ? currentFilter.filter(item => item !== filterName)
      : [...currentFilter, filterName];

    return { ...prevFilter, [filterType]: updatedFilter.length > 0 ? updatedFilter : [] };
  });
}